# Maintainer: Minecrell <minecrell@minecrell.net>
# Kernel config based on: arch/arm64/configs/msm8916_defconfig

_flavor="postmarketos-qcom-msm8916"
pkgname=linux-$_flavor
pkgver=5.11_rc5
pkgrel=1
pkgdesc="Mainline kernel fork for Qualcomm MSM8916 devices"
arch="aarch64 armv7"
url="https://github.com/msm8916-mainline/linux"
license="GPL-2.0-only"
options="!strip !check !tracedeps pmb:cross-native pmb:kconfigcheck-anbox"
makedepends="bison findutils flex installkernel openssl-dev perl gmp-dev mpc1-dev mpfr-dev"

# Architecture
case "$CARCH" in
	aarch64) _carch="arm64" ;;
	arm*)    _carch="arm" ;;
esac

# Source
_tag=v${pkgver//_/-}-msm8916
source="
	$pkgname-$_tag.tar.gz::$url/archive/$_tag.tar.gz
	config-$_flavor.aarch64
	config-$_flavor.armv7
"
builddir="$srcdir/linux-${_tag#v}"

prepare() {
	default_prepare
	cp "$srcdir/config-$_flavor.$CARCH" .config
}

build() {
	unset LDFLAGS
	make ARCH="$_carch" CC="${CC:-gcc}" \
		KBUILD_BUILD_VERSION=$((pkgrel + 1 ))
}

package() {
	mkdir -p "$pkgdir"/boot
	make zinstall modules_install dtbs_install \
		ARCH="$_carch" \
		INSTALL_PATH="$pkgdir"/boot \
		INSTALL_MOD_PATH="$pkgdir" \
		INSTALL_DTBS_PATH="$pkgdir"/usr/share/dtb
	rm -f "$pkgdir"/lib/modules/*/build "$pkgdir"/lib/modules/*/source

	install -D "$builddir"/include/config/kernel.release \
		"$pkgdir"/usr/share/kernel/$_flavor/kernel.release
}

sha512sums="16524ab5b77a17b2092d5d4537246e19bde9659fde24e922e9a7fb4052b61e2d1e4bf2d3ae8b1af540fb983c702a95c4209d0e1a1046ae6848119d31b01df31f  linux-postmarketos-qcom-msm8916-v5.11-rc5-msm8916.tar.gz
7e28eaf3a097a63e203970e3fbccfac45b061316a920d342028da89c085635d7b921a20bff44c3fbf403533a69d80ba382cd7bb2bfff8f296733d3e3ac6dc1b5  config-postmarketos-qcom-msm8916.aarch64
6c3c6505ebe38272a84ef73b7ddb090b94f1a5d572827177c4eb61db588db79bad3dc8aab59bb6405c6436bd615b1d5e2fd8642f62b2c81cf6d7b07dd6c20553  config-postmarketos-qcom-msm8916.armv7"
